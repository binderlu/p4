\documentclass[a4paper]{article}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Language and font encodings
\usepackage[english]{babel}

%% just once for barium titanite
\usepackage[version=4]{mhchem} 

\usepackage{graphicx}

%% for align env
\usepackage{amsmath}

%% for multiple cols
\usepackage{multicol}

%% Units
\usepackage{siunitx}
\sisetup{uncertainty-mode = separate}
% Hyperlinks
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{hypcap}

%% ues of caption of
\usepackage[hypcap=true]{caption}

\usepackage[capitalise, nameinlink]{cleveref}

%% we dont want to indent the paragraphs. i dont like
%% howit looks
\setlength{\parindent}{0pt} 

%% sort them like in the text
\usepackage[sorting=none]{biblatex}
%% to remove a warning 
\usepackage{csquotes}
\addbibresource{piezo.bib}

% fancy headers
\usepackage{fancyhdr}

\pagestyle{fancy}% Change page style to fancy
\fancyhf{}% Clear header/footer
\fancyhead[L]{P3: Piezoelectric effect}
\fancyhead[R]{Jan Luca Binder}
\fancyhead[C]{ETH Zürich}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.4pt}% Default \headrulewidth is 0.4pt
\renewcommand{\footrulewidth}{0.4pt}% Default \footrulewidth is 0pt

\begin{document}

\pagenumbering{roman}
\thispagestyle{empty}

\begin{titlepage}
    \centering
    {\huge{\textbf{Piezoelectric Effect}}\par} 
    \vspace{1cm}
    {\Large \textsc{ETH Zürich}\par}
    \textsc{Departement Physik}\par
    \vspace{0.7cm}
    {\Large Group}\par
    \vspace{0.2cm}
    \begin{tabular}{c c c}
        Binder, Jan Luca&\hspace{2cm}& Radu, Horia\\
        \texttt{\href{mailto://binderlu@student.ethz.ch}{binderlu@student.ethz.ch}} && \texttt{\href{mailto://horadu@student.ethz.ch}{horadu@student.ethz.ch}} \vspace{0.1cm}\\
    \end{tabular}\par
    \vspace{0.5cm}
    {\Large Author}\par
    \vspace{0.1cm}
    Binder, Jan Luca\par
    \vspace{0.1cm}
    \texttt{\href{mailto://binderlu@student.ethz.ch}{binderlu@student.ethz.ch}}
    \vspace{0.75cm}
    \begin{abstract}
        The piezo electric effect is used in many every day application like igniting a flame or making high precision mass measurements. In this experiment, we looked at the piezo electric effect of quartz \ce{SiO2} and barium titanate \ce{BaTiO3}. Especially the temperature dependence was the field of study. When measuring the piezo module of quartz at different temperatures, we found no notable dependence, since the mean of all measured values are \SI{22.8 \pm 0.6}{\pico \coulomb \per \newton}. In contrast, the piezo module of barium titanate shows a strong temperature dependence independent of polarization. The polarized barium titanate shows a maximum piezo module of \SI{17.7\pm1.7}{\nano \coulomb \per \newton} and a minimum of \SI{7.9 \pm 0.7}{\nano \coulomb \per \newton}, while the unpolarized has a maximum piezo module of \SI{1.54\pm 0.08}{\nano \coulomb \per \newton} and a minimum of \SI{0.54\pm 0.04}{\nano \coulomb \per \newton}. We also noticed that both unpolarized and polarized barium titanate show significant higher piezo modules than quartz. 
    \end{abstract}
    \vspace{\fill}
	{\large \today\par}
\end{titlepage}

\tableofcontents
\newpage

\pagenumbering{arabic}

\begin{multicols}{2}
\section{Introduction}
The piezoelectricity plays an important role in many day-to-day objects. Crystals that exhibit the piezoelectric effect are being used in gas lighters to ignite a flame, or as a sensor in a contact microphone or in highly sensitive scales \cite{p3_manual}\cite{wiki_piezoelectricity}. Since these crystals resonate at a very specific frequency, they are used as a crystal oscillator in watches, cellphones, computers, and radios \cite{wiki_oscillator}. 

To be able to create these objects, great understanding is needed about the crystals and the underlying effect. In this experiment, we want to experimentally prove the linear dependence between the amount of charge on a crystal of quartz \ce{SiO2} in relation to the force applied on it. Additionally, we want to analyse how the temperature effects the piezoelectricity by measuring the piezo module. We want to repeat the same process with barium titanite \ce{BaTiO3}. 
\section{Theory}
\subsection{Elasticity of Lattices}
When applying a force on a homogenous material, it will get deformed. We describe this deformation with a deformation tensor $S_i$ with $i=1,\dots,6$, where $S_i$ describes deformations in the $x-$, $y-$, $z-$ direction and shear forces orthogonal to the coordinate axis. 

The tensor is connected to the stress tensor, $T_j$ which describes the force that we apply on the crystal by Hooke's law.
\begin{equation}
    S_i= \sum_{j=1}^{6}s_{ij}T_{j}
\end{equation}
where $s_{ij}$ is a matrix of the elastic constants \cite{p3_manual}.

\subsection{Piezoelectric effect}
When deforming the crystal, the polarization density $P$ inside the crystal changes. The polarization and the stress tensor are connected via 
\begin{equation}
    P_i = \sum_{j=1}^6 d_{ij}T_j
\end{equation}
where $d_{ij}$ a matrix, of the piezo modules. This is a well known literature value. 
We can detect the change of the polarization by looking at the surface charge $Q_i$. 
\begin{equation}
    \label{eq:charge_stress}
    Q_i=\sum_{j=1}^6 d_{ij}K_j
\end{equation}
where $K_j$ describes the forces that we apply on the crystal \cite{p3_manual}.
\subsubsection{Quartz}
Quartz \ce{SiO2} has the following matrix of the piezo modules: 
\begin{equation}
    (d_ij) = \begin{pmatrix}
    %    d_{11} & -d_{11} & 0 & d_{14} & 0       & 0 \\
    %    0      & 0       & 0 & 0      & -d_{14} & -2d_{11} \\
    %    0      & 0       & 0 & 0      & 0       & 0 
    d_{11} & -d_{11} & 0 & \cdots \\
    0 & 0 & 0 & \cdots \\
    0 & 0 & 0 & \cdots 
    \end{pmatrix}
\end{equation}
Since our quartz crystal in question is cut in the $x-$ direction, we will only be focusing on the $x$ component. We can now simplify \cref{eq:charge_stress} to 
\begin{equation}
    \label{eq:linear_charge_stress}
    Q_x = d_{11} K_x
\end{equation}
Since the crystal structure stays the same in our temperature regime, we would expect the measured $d_{11}$ to stay constant.
\subsubsection{Barium Titanate}
The same derivation can be made for Barium Titanate \ce{BaTiO3}. The key difference is that barium titanate has 4 phases, where two of these phases are relevant for this experiment (see \cref{tab:batio_relavant_phases}). 
\begin{center}
    \captionsetup{type=table}
    \capstart
    \begin{tabular}{c|c}
        above $\SI{120}{\celsius}$ & cubic \\ \hline
        between $\SI{120}{\celsius}$ and $\SI{5}{\celsius}$ & tetragonal
    \end{tabular}
    \caption{The 2 relevant phases of barium titanate}
    \label{tab:batio_relavant_phases}
\end{center}
Normally, our crystal is made up of many small crystals whose polarizations vectors can have many possible orientations. In this case the crystal not polarized and the effect of the piezo electric effect is small. 
Since the phases under $\SI{120}{\celsius}$ are ferroelectric, we can polarize the crystal by applying a strong electric field. When polarized, the piezo electric effect is stronger.
When heated above $\SI{120}{\celsius}$, these effects disappear. Due to impurities in our sample, this may not be a hard border but rather a blend. 
\subsection{Gauge constant}
Since the charge on the outside of the crystal can't be measured directly, we want to measure the average current. We therefore start with inserting the capacitor equation into \cref{eq:linear_charge_stress} and get 
\begin{equation}
    V\cdot C =d_{11} \cdot K_x
\end{equation}
where $V$ is the voltage and $C$ the capacity of the crystal. In this experiment, we use an engine which applies a periodic force $K_0\cdot \cos(\omega t + \varphi)$ onto a lever. This results in 
\begin{equation}
    V\cdot C = d_{11} \cdot K_0\cdot \cos(\omega t + \varphi)
\end{equation}
Where $K_0 = \frac{\SI{50}{\centi \meter}-x}{\SI{25}{\centi \meter}-x}$ encapsulates the leverage factor. The $\SI{50}{\centi \meter}$ describe the maximum length of the lever and $x$ is the position of the fulcrum.

Adding the amplification factor $g=\frac{1}{R}$  of the measuring amplifier, and taking the time average gets us 
\begin{equation}
    \label{eq:stuff_or_so_on}
    I = \frac{d_{11} \cdot g}{C} \cdot \langle K \rangle
\end{equation}
Due to gravitational or other external forces, the current may not be 0 even if the engine is turned off. Since we know that the parasitic current is constant due to the linear relation in \ref{eq:stuff_or_so_on}, we solve this by measuring the difference in current $\Delta I$ as a function of the average force $\Delta K$ applied by the engine.
\begin{equation}
    \frac{1}{C} = \frac{\Delta I}{\Delta K} \frac{1}{d_{11}\cdot g}
\end{equation}
The only quantities that we are interested in are $\frac{1}{C}$ which we call the gauge constant $\eta_g$ and $d_{11}$
\begin{align}
    \eta_g &=  \frac{\Delta I}{\Delta K} \frac{1}{d_{11}\cdot g}\\
     d_{11}&=  \frac{\Delta I}{\Delta K} \frac{1}{\eta_g\cdot g}
\end{align}
Due to the temperature dependence off $\eta_{\ce{BaTiO3}}$ we won't measure the piezo electric gauge constant directly. Instead, we calculate the constant under the assumption that both crystals have the same dimensions.  Using $C=\varepsilon_0 \varepsilon_r \frac{A}{d}$ we get the following relation
\begin{equation}
    \frac{\eta_{\ce{BaTiO3}}}{\eta_{\ce{SiO2}}} = \frac{C_{\ce{SiO2}}}{C_{\ce{BaTiO3}}} = \frac{\varepsilon_{\ce{SiO2}}}{\varepsilon_{\ce{BaTiO3}}(T)}
\end{equation}
In our temperature range, we can assume $\varepsilon_{\ce{SiO2}}$ to be constant \cite{constant_quartz_dielectric}:
\begin{equation}
    \eta_{\ce{BaTiO3}}(T) = \eta_{\ce{SiO2}} \cdot \frac{\varepsilon_{\ce{SiO2}}}{\varepsilon_{\ce{BaTiO3}}(T)}
    \label{eq:gauge_bati}
\end{equation}
\section{Experiment}
\subsection{Setup}
To be able to measure the voltage while applying force, we clamp the crystal between two copper plates. The copper plates are then wired up into a measuring amplifier which converts the voltage into a current given by the amplification factor $g$, which then can be read off an analogue pointer display.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.9\linewidth]{setup_piezo.png}
    \caption{Schematic layout of the experiment from \cite{p3_manual}}
    \label{fig:setup_piezo}
\end{center}
The force is applied to the upper copper plate via a lever. To change the amount of force that is exerted, the leverage point can be moved around. The lever is also connected to a motor via a spring, which rotates at $\SI{5}{\hertz}$.

Additionally, the copper plates and the crystal are surrounded by an oven which can be heated up to $\SI{180}{\celsius}$. 

The schematic layout can be seen here \cref{fig:setup_piezo}
\subsection{Procedure}
\subsubsection{Calibration}
We first start by calibrating the maximum range of the lever. To achieve this, we first put in a crystal which has piezoelectric properties (like quartz) and move the fulcrum in small steps. For every step, we activate the motor to apply a force and measure the current. While we measure the current, we look out for a harsh drop, from which we can infer that the fulcrum is too close to the connection of the copper plates.
\subsubsection{Quartz}
We may then start by measuring the gauge constant. Before we can start the measurement series, we have to calibrate the amplification factor $g$ by setting the fulcrum to the maximum position and setting $g$ until the meter pointer of the measuring amplifier is no longer completely deflected. 

The force exerted by the lever is not a linear, which means that moving the fulcrum a small amount could result in a large increase in force. Since we want to measure a linear dependence between force and charge, we needed equidistant points in the force space. To reach that goal, we calculated the inverse function and inserted equidistant points:
\begin{align}
    f(x) &= \frac{\SI{50}{\centi \meter}-x}{\SI{25}{\centi \meter} -  x} 
    \\\iff
    f^{-1}(x)&=25\cdot\frac{x-\SI{2}{\centi \meter}}{x-\SI{1}{\centi \meter}}
\end{align}
After calculating the fulcrum points, we started with the first measuring series by keeping the quartz crystal at room temperature. We then used the following scheme:
\begin{enumerate}
    \item Set the fulcrum position.
    \item Activate the motor.
    \item Read off the current from the amplifier.
    \item Turn off the motor.
    \item Repeat until no more points left.
\end{enumerate}
Next, we wanted to test out the temperature dependence of the piezo module. To achieve this, we let the oven heat up to about $\SI{120}{\celsius}$ and let it cool down. During the cooldown phase, we made measurements at two fulcrum points every $\SI{10}{\celsius}$.
\subsubsection{Barium Titanate}
When the oven reached a low temperature, we deactivated any electronic connections to the setup and opened the oven. We then exchange the quartz crystal with the barium titanate, which we cleaned thoroughly with alcohol beforehand. 

Since we only wanted to prove a temperate dependence of the piezo module, we didn't make the measurements for the gauge constant. Instead, we started by heating the oven up to $\SI{130}{\celsius}$ and turning on the strong electric field at around $\SI{110}{\celsius}$ to polarize the crystal. After it cooled down to about $\SI{110}{\celsius}$, we deactivated the electric field and calibrated the amplification factor $g$, just as described above. 

This time, we measured at 6 fulcrum points every $\SI{10}{\celsius}$. 

When reaching room temperature, we heated up the oven back to $\SI{130}{\celsius}$, this time not turning on the electric field. The piezo electric effect of the now unpolarised crystal was measured in the same way as the polarized one.
\section{Results}
\subsection{Calibration}
We took 21 measurements at approximately equal distances. At around $\SI{21.5}{\centi \meter}$, we noticed a sharp decline in current. Since the maximum can be set arbitrarily without much impact on the other measurements, we chose $\SI{21}{\centi \meter}$ since it can still provide a high leverage factor of $7.25$. We further justify this by being a reasonable distance away from the cut-off point, so we can be sure that there will be no border effects on our measurements.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{fulcrum_calibration.pdf}
    \caption{Plot of the calibration points. On the x-axis is the fulcrum position in $\si{\centi \meter}$ and on the y-axis the read-off current in $\si{\micro \ampere}$. In red is our chosen maximum fulcrum point in $\si{\centi \meter}$.}
\end{center}
We didn't include any errors in this measurement, since they are negligible.
\subsection{Gauge Constant of Quartz}
First, we calibrated the amplification factor and found out a value of $g=\SI{20}{\per \ohm}$ would result in a range where the maximum leverage factor would not exceed the scale of the measuring equipment.

\subsubsection{At room temperature}
We took 21 measurements, which can be seen in \cref{fig:quartz_gauge_const_room_temp}:
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{quartz_20deg.pdf}
    \caption{Measurement of the gauge constant at room temperature. }
    \label{fig:quartz_gauge_const_room_temp}
\end{center}
The error in the leverage factor arises due to human error of not being able to set the fulcrum at the precise position and other possible structural errors. We estimate the error to be $\Delta l=\SI{0.05}{\centi\meter}$, which results in a maximum error of $\Delta L = \SI{0.08}{}$ of the leverage factor. 
We chose $\Delta l$ since the ruler markings are subdivided into \SI{0.1}{\centi \meter}.

The voltage error arises from the analogue parts and when reading off the analogue pointer. We estimate this to be $\Delta I = \SI{0.2}{\micro\ampere}$. Using Gaussian error propagation, this results in a maximum error of $\Delta V = \SI{0.01}{\volt}$

To acquire the gauge constant, we fitted a linear function over the points, as can be seen in \cref{fig:quartz_gauge_const_room_temp}. Using $d_{11}=\SI{2.3}{\pico\coulomb\per\newton}$ \cite{d11_value} as the piezo module for quartz results in 
\begin{equation}
    \eta = \SI{3.20 \pm 0.08 e5}{\per\farad}
    \label{eq:quartz_gauge}
\end{equation}
Calculating the $R^2$ value results in 
\begin{equation}
    R^2\approx 0.99
\end{equation}

\subsubsection{At varying temperatures}
\label{sec:quartz_var_temp}
At each temperature point, we made a measurement at $\SI{0}{\centi\meter}$ and $\SI{11}{\centi\meter}$. The first measurement point is $\SI{0}{\centi\meter}$ because it could be quickly adjusted. Since the second measurement can be set arbitrarily, we chose $\SI{11}{\centi\meter}$, due to it achieving the most consistent results by being far away from the maximum fulcrum point. The results can be seen in \cref{fig:quartz_gauge_const_var_temp}:
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{d11 quartz.pdf}
    \caption{Measurement of the piezo module at varying temperatures reaching from $\SI{26}{\celsius}$ up to $\SI{120}{\celsius}$. The dotted orange line shows the literature value of $d_{11}$ and the green dashed line shows the linear fit}
    \label{fig:quartz_gauge_const_var_temp}
\end{center}
To see if we have a dependence on the temperature, a linear fit was made across all measured points. The $R^2$ value is
\begin{equation}
    R^2 \approx 0.25
\end{equation}

While adjusting the fulcrum and measuring the charge of the crystal, the temperature steadily  dropped. This resulted in an estimated error of $\Delta T =  \SI{1}{\celsius}$. 
\subsection{Barium Titanate}
Since the gauge constant of barium titanate is strongly temperature dependent, we used the \cref{eq:gauge_bati} to calculate it at the 7 temperature points we took. We used $\varepsilon_{\ce{SiO2}} = 4.9$ \cite{quartz_permitivity}, the gauge constant calculated in \cref{fig:quartz_gauge_const_room_temp} and the relative permittivity of \ce{BaTiO3} described in \cite{dielectric_props}, which resulted in \cref{fig:gauge_batio3}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{gauge_batio3.pdf}
    \caption{Using literature values of the relative permittivity of quart and barium titanate and the calculated gauge constant of quartz, we plotted the temperature dependent gauge constant of barium titanate.}
    \label{fig:gauge_batio3}
\end{center}
Using the gauge constant, we could now calculate the piezo module.
\subsection{Polarized Barium Titanate}
\label{sec:44}
We again calibrated the amplification factor and found out that $g=\SI{1}{\per\ohm}$ resulted in the best amplification.

We measured while cooling the barium titanate at 7 different temperatures. At any temperature, we calculated at 6 different leverage points. The measurements can be seen in \cref{fig:barium_titanate_polarized}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.9\linewidth]{barium titanate polarized.pdf}
    \caption{All measurements of the polarized barium titanate at varying temperatures.}
    \label{fig:barium_titanate_polarized}
\end{center}
We again linearly fitted all points and plotted the results against temperature in \cref{fig:pol_bati_d33}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{pol_bati_d33.pdf}
    \caption{Plotted are the piezo module calculated using the gauge constant, amplification factor and the measurements from \cref{fig:barium_titanate_polarized} }
    \label{fig:pol_bati_d33}
\end{center}
The error are a result of using Gaussian error propagation, where we used the error from the gauge constant and on the other hand the auto covariance from the linear fit. 

The error in the $x$-direction has been estimated to be around $\pm\SI{1}{\celsius}$. We use this value due to the crystal cooling down while making the measurements and to the estimated uncertainty of the temperature sensor.

\subsection{Unpolarized Barium Titanate}
After heating the crystal up to $\SI{130}{\celsius}$ and not introducing a strong electric field, we calibrated the amplification factor and got $g=\SI{10}{\per\ohm}$. 

We measured the points like in \cref{sec:44} by letting the crystal cool down and measuring 6 points every $\SI{10}{\celsius}$ as can be seen in \cref{fig:barium_titanate_unpolarized}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{barium titanate unpolarized.pdf}
    \caption{All measurements of the unpolarized barium titanate.}
    \label{fig:barium_titanate_unpolarized}
\end{center}
Linearly fitting at each temperature results in plot \cref{fig:no_pol_bati_d33}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{no_pol_bati_d33.pdf}
    \caption{Same measurement technique as the polarized crystal: Measuring at 7 different temperature points and plotting the gradient.}
    \label{fig:no_pol_bati_d33}
\end{center}

\section{Discussion}
In general, we can conclude that the data we measured in part agrees with the literature.
\subsection{Calibration}
We acknowledge that the calibration was done quite imprecise. We can justify this approach, since we know that the error in $x$-direction is small $<\SI{0.05}{\centi\meter}$. The cut-off was also chosen while keeping the error in mind, meaning that any imprecision would not have any effect on the end result. 

Additionally, the cut-off could be chosen quite arbitrarily, as long as we have a wide range of fulcrum positions to work with. Since the leverage factor ranges from 2 to 7.25 we think that this is enough to justify the calibration and to move on to the measuring of the data.

In general, the calibration went on smoothly and quickly without any majors problems.

\subsection{Gauge Constant of Quartz}
We justify the calibration of the amplification factor by looking at high and low leverage factors and noticing that both would results in a deflection of the pointer on the measuring amplifier that could easily be read off. 
Additionally, we acknowledge the read off errors when working with lower leverage factors due to the smaller deflection in general, but also believe that the error introduced by this is negligible. 
\subsubsection{Room temperature}
Calculating the gauge constant at room temperature was also straight forward. The data points that can be seen in \cref{fig:quartz_gauge_const_room_temp}, strongly suggest a linear relation between the leverage factor and the voltage. This claim is also supported by the fact that the $R^2$ value nearly indicates a perfect fit. Additionally, the error of the data points are in general low. Especially in the $y$-direction, where the error bars cannot be seen.

The error of the gauge constant \cref{eq:quartz_gauge} can also be considered low, and therefore the value of the gauge constant accurate.

An unknown error source for which we couldn't estimate the error accurately is the measuring amplifier. We estimated that the amplifier has a lower uncertainty than the readout, which was an analogue pointer that is prone to human errors.
\subsubsection{Varying temperatures}
Looking at \cref{fig:quartz_gauge_const_var_temp}, one can directly notice that the data points seem to be distributed randomly around the literature $d_{11}$ value of quartz. Additionally, some sort of binning can be observed. 

The binning can be explained by the low amount of data points we took at each temperature step. Since we only took two data points at each step, this results in a lower resolution of the linear fit, where the variance can not be estimated. This also means that even if the error bars in the plot are correctly calculated, the real error could be higher.

Even with the shortcomings, we are still able to make predictions about the temperature dependence.

We could still perform a linear fit over the data points and notice an increase in the piezo module when the temperature increases. Making a prediction about the temperature dependence can not be justified since the quality of the linear fit is of poor quality, as can be in seen in the $R^2$ value of $R^2\approx 0.25$. This either means that there is no linear dependence or more possibly no temperature dependence at all.

Sadly, both of the cases would not agree with the literature, which predicts a decline in piezo module when increasing the temperature \cite{quatz_hallo_ich_bin_sanchu}. The reason for disagreement could be due to the strong binning explained before or due to the impurities of the quartz crystal. 

\subsection{Barium Titanate}
To do any meaningful measurements, we first had to calculate the strong temperature dependent gauge constant. To achieve this, we used the experimental results from \cref{sec:quartz_var_temp} and assumed the piezo module and therefore the gauge constant of quartz to be constant. We justify this ansatz by only looking in a temperature regime where the gauge constant doesn't change much.

\subsubsection{Polarized Barium Titanate}
When calibrating the amplification factor $g$, we immediately noticed that the polarized crystal show stronger piezo electric effect than quartz.  Using the same reasoning as before, we set the amplification factor to $1$.

Since the data in \cref{fig:quartz_gauge_const_var_temp} shows strong binning, we used more data points per temperature step. This tradeoff results in a larger uncertainty in the temperature, since it takes longer to measure all points. Despite that, we could still achieve a relatively small error in temperature, which means that the data we recorded could still be used to make prediction about the crystal.

The advantage of making multiple measurements means that the variance can be approximated. As a result of this, the errors shown in \cref{fig:pol_bati_d33} are more accurate than the errors shown in \cref{fig:quartz_gauge_const_var_temp}. Since we could estimate the variance, we omitted the calculation of the $R^2$ values. 

The plot shows a low piezo module which increases sharply with temperature, around the \SI{100}{\celsius} mark. Considering the errors, we can confidently say that the piezo module decreases with increasing temperature.

\subsubsection{Unpolarized Barium Titanate}
We notice that the piezo constant is $10$ times bigger than the piezo constant of the polarized crystal. From this, we can postulate that the piezo electric effects from the polarized crystal is stronger in comparison to the unpolarized crystal.

To support this claim, we can look at \cref{fig:no_pol_bati_d33} where we can see that the data points show a lower piezo module at the same temperature as in \cref{fig:pol_bati_d33}.

We also notice that the form of the two graphs have the same characteristics. Both have a low piezo module at lower temperature but rise steadily at \SI{90}{\celsius}. This also supports the theory since in the unpolarized phase less polarization vectors are showing in the same direction which results in a lower module. Since there are still some that are showing in the correct direction, a small piezo electric effect can still be measured.  

We should also note that both the unpolarized and polarized barium titanate crystals are both stronger than the quartz crystal.

\section{Problems}
Even though the experiment went smoothly, there are still areas that can be improved to get better data.
\begin{itemize}
    \item \textbf{Temperature variance}: The biggest problem we encountered was the variance in temperature. Since we measured the piezo electric effects from high temperature to low temperature and the oven was too slow, the temperature wasn't constant while measuring the data points. This could easily be avoided by using an oven which has fine control over the temperature.

    Due to an oversight on our part, we are not able to know the exact Temperature error at each temperature step. Since we did record the maximum temperature variance to be \SI{1}{\celsius}, we used this as the error in all plots.
    \item \textbf{Temperature latency}: Another problem we've encountered was with the oven. When turning off the oven, it would still take time until the crystal would stop heating up. Using another way to heat up the crystal, which would stop the heating process faster, would be beneficial. 
    \item \textbf{Measurement system}: The measurement system that we used was prone to fluctuations. Sometimes, epically when measuring the piezo module of barium titanate, it would not set. Instead, it would oscillate around the value for some time. This is not a big problem since we could just repeat the measurement. 
    \item \textbf{Lever setup}: The setup of the lever also seemed to be problematic. When measuring at the same fulcrum position twice, we would get different values. The reason for this is that the lever would not set properly on the fulcrum. This resulted in confusion and the measuring of wrong data if not being careful. We could mitigate this by shaking the lever to make sure it sets into place. The precision could be optimized by using higher quality or better lubricated joints in the lever setup. A high precision motor that would set the fulcrum position seems to be beneficial to be able to make more precise measurements.
\end{itemize}
\section{Conclusion}
We calculated with high precision the gauge factor of the quartz to be $\eta = \SI{3.20 \pm 0.08 e5}{\per\farad}$ and even if we could not get a temperature dependence of piezo module $d_{11}=\SI{22.8 \pm 0.6}{\pico \coulomb \per \newton}$ our values were still in the correct magnitude. We did show a temperature dependence of both unpolarized and polarized barium titanate, which was expected. Even though the measuring apparatus had some problem, all of them could be circumvented by measuring carefully. 

The \LaTeX{} file of this report, the data analysis and any data we measured can be found on \hyperlink{https://gitlab.ethz.ch/binderlu/p3/-/tree/master/Piezo}{GitLab}.

\end{multicols}
\newpage
\printbibliography
\end{document}