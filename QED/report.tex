\documentclass[a4paper]{article}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Language and font encodings
\usepackage[english]{babel}

%% just once for barium titanite
\usepackage[version=4]{mhchem} 

\usepackage{graphicx}

%% for align env
\usepackage{amsmath}
\usepackage{braket}

%% for multiple cols
\usepackage{multicol}

%% Units
\usepackage{siunitx}
\sisetup{uncertainty-mode = separate}
% Hyperlinks
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{hypcap}

%% ues of caption of
\usepackage[hypcap=true]{caption}

\usepackage[capitalise, nameinlink]{cleveref}

%% we dont want to indent the paragraphs. i dont like
%% howit looks
\setlength{\parindent}{0pt} 

%% sort them like in the text
\usepackage[sorting=none]{biblatex}
%% to remove a warning 
\usepackage{csquotes}
\addbibresource{quantum.bib}

% fancy headers
\usepackage{fancyhdr}

\pagestyle{fancy}% Change page style to fancy
\fancyhf{}% Clear header/footer
\fancyhead[L]{P3: Circuit QED}
\fancyhead[R]{Jan Luca Binder}
\fancyhead[C]{ETH Zürich}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.4pt}% Default \headrulewidth is 0.4pt
\renewcommand{\footrulewidth}{0.4pt}% Default \footrulewidth is 0pt

\begin{document}

\pagenumbering{roman}
\thispagestyle{empty}

\begin{titlepage}
    \centering
    {\huge{\textbf{Circuit Quantum Electrodynamics}}\par} 
    \vspace{1cm}
    {\Large \textsc{ETH Zürich}\par}
    \textsc{Quantum Device Laboratory}\par
    \vspace{0.7cm}
    {\Large Group}\par
    \vspace{0.2cm}
    \begin{tabular}{c c c}
        Binder, Jan Luca&\hspace{2cm}& Radu, Horia\\
        \texttt{\href{mailto://binderlu@student.ethz.ch}{binderlu@student.ethz.ch}} && \texttt{\href{mailto://horadu@student.ethz.ch}{horadu@student.ethz.ch}} \vspace{0.1cm}\\
    \end{tabular}\par
    \vspace{0.5cm}
    {\Large Author}\par
    \vspace{0.1cm}
    Binder, Jan Luca\par
    \vspace{0.1cm}
    \texttt{\href{mailto://binderlu@student.ethz.ch}{binderlu@student.ethz.ch}}
    \vspace{0.75cm}
    \begin{abstract}
        In this experiment, we wanted to measure the characteristics of a transmon and the copper cavity that encloses it. Measuring the resonance frequency of the cavity resulted in a value of $\SI{6.623699 \pm 0.000009}{\giga\hertz}$. Further, we found a sinusoidal dependence of the cavity when changing the magnetic flux and measured the maximum and minimum frequency of the saturated transmon. At the end of the experiment, we measured the Josephson and transmon charging energy and calculated the ratio.
    \end{abstract}
    \vspace{\fill}
	{\large \today\par}
\end{titlepage}

\tableofcontents
\newpage

\pagenumbering{arabic}

\begin{multicols}{2}
\section{Introduction}
Quantum computing is becoming more relevant than ever. Especially since quantum computers are theoretically able to process information exponentially faster than classical computers \cite{wiki_quantum_computing}.  Instead of using a bit like in classical computers, quantum computers use qubits as their unit of information. A transmon is a special kind of superconducting qubit. Crucial to this design are the Josephson junction.

The goal of this experiment is to measure the characteristics of the copper cavity and the qubit inside.
\section{Theory}
\subsection{Josephson Junction}
Since we work in a low temperature regime $> \SI{30}{\milli \kelvin}$, the Josephson junction acts as a non-linear inductor. We describe this phenomenon with 
\begin{align}
    I &= I_c\sin \phi(t)\\
    V &= \frac{\Phi_0}{2\pi}\frac{\mathrm{d}\phi(t)}{\mathrm{d}t}
\end{align}
where $I$ is the current and $V$ the voltage across the junction. $I_c$ is the junction critical current, $\Phi_0=\frac{h}{2e}$ and $\phi$ the phase difference. From this we can follow that the energy is described by
\begin{equation}
    E=-E_J \cos\left(2\pi \frac{\Phi_0}{\Phi}\right)
\end{equation}
where $E_J = \Phi_0 \frac{I_c}{2\pi}$ is the Josephson energy.
The transom is formed by connecting a Josephson junction in parallel with a capacitor. From this we can write the Hamiltonian
\begin{align}
    \hat{H}_T &=H_{\mathrm{el}}+H_{\mathrm{mag}} \\
              &= 4 E_C(\hat{n}-n_g)^2-E_J\cos\hat{\phi}
\end{align}
where $\hat{n}$ is the charge number operator, $n_g$ is an offset charge and $E_C$ is the transmon charging energy.

Instead of a quadratic potential of an LC circuit, the transmon circuit exhibits an anharmonic cosine potential. Due to the anharmonicity, we can measure the energy state of the qubit.

\subsection{SQUID}
A superconducting quantum interference device (SQUID) is made out of two Josephson junction connected in parallel. We can use the high sensitivity to magnetic fields to tune the qubit frequency.

The Hamiltonian of the transmon stays the same, but the energy is now given by 
\begin{equation}
    E_J=(E_{J_1} + E_{J_2} )\cos \left(\pi \frac{\Phi_{\mathrm{ext}}}{\Phi_0} \right)
\end{equation}
Using Mathieu functions we can solve the Hamiltonian and find the qubit frequency $\omega_q$
\begin{equation}
    \hbar \omega_q = \sqrt{8 E_C E_J \left|\cos\left(\pi \frac{\Phi_{\mathrm{ext}}}{\Phi_0}\right)\right|}- E_C
\end{equation}
\subsection{Cavity Interaction}
The qubit and the cavity are coupled with each other with the coupling strengths $g_{i,i+1}$ where $i$ describes the $i^\mathrm{th}$ excitation of the transmon. We'll denote the cavity frequency as $\omega_r$ and $g_{0, 1}$ as $g$. There are two coupled transmon cavity system of interests: the resonant case $\Delta = |\omega_r - \omega_q|=0$ and the dispersive regime $\Delta\gg g$.
\subsubsection{Resonant case}
In the resonant case, the energy states are lifted by $2g\sqrt{n}$. By using a Bogoliubov transformation and restricting the energies to the first two energy levels, we get 
\begin{align*}
    E_{g,n}&=\hbar \omega_r - \frac{\hbar}{2}\sqrt{\Delta^2+4g^2n}\\
    E_{e,n-1}&= \hbar\omega_r + \frac{\hbar}{2}\sqrt{\Delta^2+4g^2n}
\end{align*}
When probing the cavity we can look at the distance of the closest frequency separation to estimate the coupling strength $\frac{g}{\pi}$.
\subsubsection{Dispersive case}
When the cavity and the qubit are far detuned from each other, they don't share any energy. In this case, the qubit frequencies are shifted by the \textit{lamb shift} $\chi_{01} = \frac{g^2}{\Delta}$ due to vacuum fluctuations: 
\begin{equation}
    \omega_q' = \omega_q + \chi_{10}
\end{equation}
The frequency of the cavity also shifts according to $\chi_{12} = \frac{g^2_{1,2}}{2|\omega_r- \omega_{12}|}$. The new renormalized frequencies is
\begin{equation}
    \omega'_r = \omega_r - \frac{\chi_{12}}{2}
    \label{eq:chi12}
\end{equation}
This is cause since the cavity is interacting with the second excited state of the transmon.

These shifts result in a new cavity frequency depending on the state of the qubit. If the qubit is in the ground state $\ket{g}$ the frequency of the cavity is
\begin{equation}
    \hat{\omega} = \omega_r' + \chi
\end{equation}
and if in the excited state $\ket{e}$ then the cavity frequency is 
\begin{equation}
    \hat{\omega} = \omega_r' - \chi
\end{equation}


\section{Experiment}
Since the experiment depends on superconductors and high thermal noise, the setup and measuring procedures have to be designed accordingly.
\subsection{Setup}
We use a cavity made out of copper. At the side of the cavity is a hole from which the antenna protruding. The qubit is placed inside the copper cavity, which in turn is put into a system of vacuum chambers to cool it down to a working temperature.
\subsubsection{Temperature Regulation}
For the Josephson junctions to work, a working temperature of $\SI{30}{\milli \kelvin}$ is needed.

The setup consist of two vacuum chambers, the inner dewar and the surrounding outer dewar. The outer vacuum chamber is filled with \ce{^4He} such that the inner vacuum chamber is submerged. Since it is completely evacuated, we can cool the inner chamber down to $\SI{4.2}{\kelvin}$.

Inside the inner chamber, multiple steps are required to cool down to the required $\SI{30}{\milli\kelvin}$:
\begin{enumerate}
    \item \ce{^4He} is evaporated to cool down the system to $\SI{1.5}{\kelvin}$.
    \item Evaporation of a dilute \ce{^3He/^4He} mixture, using 6-7\% \ce{^3 He}. This cools the system down to $\SI{600}{\milli\kelvin}$
    \item Using dilution cooling by mixing condensed \ce{^3He} with \ce{^4He} can reach the final temperature of $\SI{30}{\milli\kelvin}$
\end{enumerate}
\subsubsection{Measuring equipment}
To read out qubit, frequencies in the GHz regime are needed. These are transmitted via a protruding antenna inside the copper cavity. Since the waveform generator only generates signals in the MHz regime, we use a radio-frequency mixer. The radio-mixer generates two sidebands in the required GHz regime. Since we want to send only one frequency to the qubit, we have to use IQ mixing. Using this, we can apply a singe tone at the qubit frequency. 

To measure the signal, we first have to use downconversion so it can be detected by the acquisition device. The acquisition device integrates the signal over $\SI{2}{\micro \second}$, which gives one acquisition shot. Since we want to look at the average, we acquire up to $2^{14}$ shots and take the mean of the obtained data.

To control all the measuring equipment, we use a Python based software framework called PycQED. Using this, we can create a script that runs all needed measurements.
\subsection{Procedure}
In this experiment, we are conducting the following measurements
\begin{enumerate}
    \item \textbf{Frequency cavity sweep}: In the first step, we want to find out how the frequency of the cavity $\omega_r$ with no external magnetic field. To achieve this, we sweep over a range of frequencies we have to determine.
    \item \textbf{Power sweep}: In this measurement, we want to find out at which power the transmon saturates and returns the frequency of the cavity. This is also known as the "qubit punch out". From this we can calculate $\chi_{12}$.
    \item \textbf{Magnetic flux sweep}: We can control the resonant frequency of the cavity by applying an external magnetic field. We want to check the sinusoidal dependence of the magnetic flux and the frequency by sweeping over a range of magnetic flux.
    \item \textbf{Qubit spectroscopy}: Next we want to look at the frequencies at which the transmon is in the ground state and excited state. For this, we apply different magnetic fluxes at low power and look where the transmon is excited. We can do this by looking at a resonant shift of the cavity.
\end{enumerate}

\subsection{Results}
\subsubsection{Resonant Frequency of the Cavity}
\label{sec:lorentzian}
We first started with a frequency sweep from $\SI{2}{\giga \hertz}$ to $\SI{10}{\giga \hertz}$ \cref{fig:freq_sweep_1}, where we noticed a peak around $\SI{6}{\giga \hertz}$ to $\SI{7}{\giga \hertz}$. We fitted the Lorentzian function over the data points, which resulted in an $R^2$ value of $\approx\SI{94.1}{\percent}$. The fitted peak is at $\SI{6.619 \pm 0.005}{\giga \hertz}$, where we use the covariance of the fit to determine the uncertainty.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/freq_sweep_1.pdf}
    \caption{Measurement ranging from $\SI{2}{\giga \hertz}$ to $\SI{10}{\giga \hertz}$ with a step size of $\Delta f = \SI{0.1}{\giga\hertz}$.}
    \label{fig:freq_sweep_1}
\end{center}
Looking more closely at the $\SI{6.25}{\giga \hertz}$ to $\SI{7}{\giga \hertz}$, we get the following data points, as can be seen in \cref{fig:freq_sweep_2}. Again, fitting the Lorentzian results in an $R^2$ value of $\approx\SI{95.2}{\percent}$. This time, the fitted peak is at $\SI{6.623699 \pm 0.000009}{\giga\hertz}$
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/freq_sweep_2.pdf}
    \caption{Second cavity sweep, ranging from $\SI{6.25}{\giga \hertz}$ to $\SI{7}{\giga \hertz}$. The step size is $\Delta f=\SI{10}{\mega\hertz}$. Note that only every 20\textsuperscript{th} data point is shown in the plot}
    \label{fig:freq_sweep_2}
\end{center}
We want to point out that in both cases, the resonant frequency of the cavity was calculated to be at $\omega_r\approx \SI{6.62}{\giga\hertz}$
\subsubsection{Qubit Punch Out}
After measuring the resonant frequency, we probed for the maximum power that can be delivered to the qubit without saturating it. 
All measurements were taken at a flux voltage of $\SI{-2.5}{\volt}$, since at this voltage the qubit frequency was approximately the furthest away from the cavity frequency.

Since we don't know what at what range the punch out would occur, we did a wide power sweep that can be seen in \cref{fig:power_sweep_wide}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/wide_power_sweep.pdf}
    \caption{First power sweep with a maximum pulse amplitude of $\SI{248}{\milli\volt}$ and a step of $\Delta V=\SI{2}{\milli \volt}$. The frequency ranges from $\SI{6.55}{\giga\hertz}$ up to $\SI{6.67}{\giga\hertz}$ with $\SI{500}{\kilo \hertz}$ steps.}
    \label{fig:power_sweep_wide}
\end{center}
From this measurement, we could estimate that the bare cavity frequency is at $\SI{6.6230 \pm 0.0005}{\giga\hertz}$ and the maximum transom frequency at $\SI{6.6305 \pm 0.0005}{\giga\hertz}$. Using \cref{eq:chi12} gets us 
\begin{equation}
    \chi_{01} - \chi_{12} = \SI{7.5 \pm 0.7}{\mega \hertz}
\end{equation}
Since we could see a punch out at around \SI{50}{\milli\volt}, we did the next measurement more precisely in the neighbourhood which can be seen in \cref{fig:power_sweep_narrow}.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/narrow_power_sweep.pdf}
    \caption{Wider power sweep with a maximum pulse amplitude of $\SI{49.5}{\milli\volt}$ and a step of $\Delta V=\SI{0.5}{\milli \volt}$. The frequency ranges from $\SI{6.61}{\giga\hertz}$ up to $\SI{6.64}{\giga\hertz}$ with $\SI{150}{\kilo \hertz}$ steps.}
    \label{fig:power_sweep_narrow}
\end{center}
In this measurement, the cavity frequency was measured around $\SI{6.62320 \pm 0.00010}{\giga\hertz}$ and the transmon frequency was $\SI{6.63090 \pm 0.00010}{\giga\hertz}$. This results in
\begin{equation}
    \chi_{01} - \chi_{12} = \SI{7.70 \pm 0.14}{\mega\hertz}
\end{equation}
In both calculation the error is estimated using the resolution of the measurements.
\subsubsection{Flux sweep}
Between the qubit punch out measurements, we made additional measurements of the magnetic flux against the frequency of the transmon. 
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/magnetic_flux_sweep1.pdf}
    \caption{Magnetic flux sweep using a flux voltage ranging from \SI{-6}{\volt} to \SI{7.8}{\volt} with a resolution of \SI{0.2}{\volt}. The frequency has a resolution of \SI{200}{\kilo\hertz}.}
    \label{fig:flux_sweep1}
\end{center}
Fitting a sinusoidal function over the data points results in a $R^2$ value of $\approx\SI{92.8}{\percent}$. Using the sinusoidal fit, we find the upper sweet spot of the transmon, or the frequency maximum, at \SI{-2.69 \pm 0.010}{\volt} with a frequency of \SI{6.62919 \pm 0.00012}{\giga\hertz}. 

The minimal frequency is predicted at \SI{2.75 \pm 0.06}{\volt} with a frequency of \SI{-6.62347 \pm 0.00012}{\giga\hertz}.

\subsubsection{Qubit spectroscopy}
After reducing the power to a level where only one peak can be detected, we found the resonant frequency of the transmon at $\approx\SI{5.3}{\giga\hertz}$. We used this information to probe the qubit at different frequencies and got the following data, which can be seen in \cref{fig:qubit_spec1} and \cref{fig:qubit_spec2}. The calculated $R^2$ are shown in the respective figures.
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/qubit_spec1.pdf}
    \caption{In this measurement, we took a flux step size of $\SI{0.2}{\volt}$ and frequency step size of \SI{10}{\mega\hertz}.}
    \label{fig:qubit_spec1}
\end{center}
Using the parameters of the fit, we can estimate the individual energies $E_C$ and $E_J$ and from this we get the energy ratio $\frac{E_J}{E_C}$. In the first measurement, we got a value of 
\begin{align}
    E_J &= \SI{-6.11 \pm 0.35 1e-27}{\joule}\\
    E_C &= \SI{-4.111 \pm 0.033 1e-25}{\joule}
\end{align}
Using these values we can calculate the ratio
\begin{equation}
    \frac{E_J}{E_C} = 0.0149\pm0.0009
\end{equation}
\begin{center}
    \captionsetup{type=figure}
    \capstart
    \includegraphics[width=0.95\linewidth]{QED/rsc/qubit_spec2.pdf}
    \caption{Qubit spectroscopy with a voltage resolution of \SI{0.1}{\volt} and a frequency resolution of \SI{5}{\mega\hertz}. The yellow part on the top shows missing data.}
    \label{fig:qubit_spec2}
\end{center}
From the second fit, we get the following:
\begin{align}
    E_J &= \SI{-4.160 \pm 0.032 1e-25}{\joule}\\
    E_C &= \SI{-5.69 \pm 0.35 1e-27}{\joule}
\end{align}
with following ratio
\begin{equation}
    \frac{E_J}{E_C} = 0.0137\pm0.0009
\end{equation}
Note that the measurement shown in \cref{fig:qubit_spec2} is not complete, since the measurement crashed at the end. Since the most important part of the data was already collected, we still decided to use the dataset in our results. 

\subsection{Data Analysis and Discussion}
\subsubsection{Frequency analysis of the Cavity}
We notice in both \cref{fig:freq_sweep_1} and \cref{fig:freq_sweep_2} that the Lorentzian fitted has relatively small y-offset. Since we are only interested in the resonant frequency of the cavity, we can safely ignore this. We justify both fits by looking at the $R^2$ values. Since both of the $R^2$ values are higher than $\SI{94}{\percent}$, we can say that the fit fits well on the measured data. 

Additionally, the measured resonant frequencies from the first measurement are compatible to the measurement of the second one. This is due to the fact that the first measured value plus the standard deviation intersects the value of the second measurement. Expectantly, we find that the value of the second measurement is not compatible with the first measurement. This makes sense since the standard deviation in the second measurement is relatively small in relation to the first one.

\subsubsection{Punch out}
When analysing the data for the punch out, we want to follow the maximum magnitude at each pulse amplitude step. To achieve this, we iterated through each pulse amplitude voltage step and calculated a moving average. In \cref{fig:power_sweep_wide} a moving average of 2 and in \cref{fig:power_sweep_narrow} a moving average window of 10 were used.

After calculating the moving average, we used the maximum magnitude to choose the resonance frequency at each pulse amplitude step. This can be seen in both figures as the red dashed line. 

Using these values, we could easily calculate the maximum and the minimum resonance frequency.

Since we estimate that the uncertainty of the measuring equipment is negligibly small, we estimate that the uncertainty comes from the finite frequency step. 

In the end, we notice that the results from both measurements agree with each other. When looking at the expectation value and uncertainty of the first measurement, we see that they are fully compatible with the second measurement. From this consistency, we can follow that the results we calculated are not wrong.

\subsubsection{Flux sweep}
In the flux sweep, we again looked at each flux voltage step separately. At each step we calculated a moving average window of 10 and determined the maximum. The maxima are plotted as blue crosses in \cref{fig:flux_sweep1}. In the next step, we fitted a sinusoidal function over all maxima, which can be seen as the dashed red line in the same plot. 

The $R^2$ value is rather low compared to the Lorentzian, fits in \cref{sec:lorentzian}. We can explain this discrepancy by noting that the measuring equipment sometimes measured didn't measure correctly and instead returned noisy data. This leads to randomly distributed maxima, which in turn explains the rather low $R^2$ value.

We could now use the sinusoidal function to calculate a predicted maximum and minimum. The uncertainties are calculated using the predicted covariance from the fit function, which then are reflected in the uncertainty of the maximum and minimum.

\subsubsection{Qubit spectroscopy}
Analysing the data proved to be quite cumbersome. Again, we needed to calculate all peaks at each flux voltage step. 

To achieve this, we first tried using an algorithm which tries to follow a peak by searching the next peak in the neighbourhood of the last peek. Sadly, this didn't work since the minima are too close to each other at $\SI{-2.5}{\volt}$ and just jumped to another minima. Another problem was the high noise in the neighbourhoods, which meant that unwanted minima were chosen.

To combat this problem, we additionally looked if the magnitude was close to the magnitude of the last step. This also didn't work since the magnitude was inconstant between steps. This again led to jumping to another frequency.

To finally extract the minima, we used a plot digitizer software to pick the initial minima. After which we defined a neighbourhood at all initial minima and calculated the local minima. 

Using the calculated data points, we could now fit the squared cosine functions. We use the fitted parameter, we calculate the energies $E_J$ and $E_C$ where the uncertainty was again estimated by the covariance of the parameters.

Even though the fit have high $R^2$ values, even reaching an estimated $R^2$ of 1 in \cref{fig:qubit_spec1}, the fits are still poorly done. The reason for this is that the frequency range in which we measured the flux voltages is too small. Due to the small measuring window, we expect that the fitted cosine functions inflects too early.

Since the data points are missing and changing the fitted function proves to be difficult, we have to make due with the poorly fitted plot. We expect that the poorly fitted plot and additional noise is the reason that the ratio $\frac{E_J}{E_C}$ doesn't have the expected value of approximately 50. 

Interestingly, calculating the reciprocal $\frac{E_C}{E_J}$ results in a value that is close to the expected value. For the measurement in \cref{fig:qubit_spec1}, we get a value of $67\pm4$ and for \cref{fig:qubit_spec2} we get $73\pm5$. This result would indicate that the calculation of the energy maybe be faulty, but after rigorously rechecking all formulas, we couldn't find any mistakes. Additionally, the fact that we calculated negative energies also indicate that our calculations are indeed correct.

\section{Problems}
We encountered multiple problems during our experiment
\begin{itemize}
    \item \textbf{Measurment equipment disconnecting}: When measuring for a long period of time the measuring equipment seem to disconnect often. This resulted in incomplete data that could often not be used or recovered. 
    \item \textbf{Measurements taking time}: Measuring at high resolution takes a long time. Due to this, we couldn't know if our measurements could be used until the measurement was done.
    \item \textbf{Noise}: Sometimes the measuring equipment didn't measure the expected data, but instead measured plain noise. This resulted in parts of the data set being unusable.
\end{itemize}

\section{Conclusion}
Looking at all results, we conclude that all experiments except for the qubit spectroscopy resulted in sensible data. Sadly, the data measured for the qubit spectroscopy turned out to be inconclusive. 

In the first parts of the experiment, we found the resonance frequency of the cavity, measured a punch out of the qubit at high enough powers and found a sinusoidal dependency of the cavity at different flux voltages.

Since the dataset for the qubit spectroscopy is inclusive, we recommend looking at a wider range of either frequencies or flux voltages. We additionally advocate for a more consistent measuring equipment that is able to continuously record data without disconnecting.

The {\LaTeX} file, data analysis and recorded data is hosted on GitLab and can be found \href{https://gitlab.ethz.ch/binderlu/p3/-/tree/master/QED}{here}

\end{multicols}
\newpage
\printbibliography
\end{document}